package pl.sda.poznan.stringoperation;

import java.util.List;
import pl.sda.poznan.stringoperation.file.TextReader;

public class Application {

  public static void main(String[] args) throws InterruptedException {
    List<String> stringList = TextReader
        .readFromFile("C:\\Users\\siwipi\\java\\sda-introduction-to-jvm\\tekst.txt");

    // lambda expression
//    stringList.forEach(e -> System.out.println(e));

    // method reference
//    stringList.forEach(System.out::println);

    // for - each

//    String text = null;
//    for (String s : stringList) {
//      System.out.println("jestem w petli");
//      Thread.sleep(200);
//      text += s;
//    }

    // z uzyciem string builder
    StringBuilder builder = new StringBuilder();
    for (String line : stringList) {
      Thread.sleep(100);
      builder.append(line).append("\n");
    }
    String text = builder.toString();

    System.out.println(text);
  }
}
